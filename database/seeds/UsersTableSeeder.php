<?php
/**
 * Created by PhpStorm.
 * User: tsonev
 * Date: 16.09.17
 * Time: 16:27
 */

use Illuminate\Database\Seeder;
use App\Models\IncomeItem;
use App\Models\ExpenseItem;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([

	        'email' => 'tsonevpe@gmail.com',
	        'password' => app('hash')->make('0877462575.a'),
	        'name' => 'Petyo Tsonev'

        ]);

        DB::table('income_items')->insert([

        	[
		        'user_id' => 1,
		        'name' => 'Заплата',
		        'description' => 'Заплата от WebMotion'
	        ],
	        [
		        'user_id' => 1,
		        'name' => 'Дамски Превръзки',
		        'description' => 'Продажба'
	        ],
	        [
		        'user_id' => 1,
		        'name' => 'Заем',
		        'description' => 'Взети Заеми'
	        ],
	        [
		        'user_id' => 1,
		        'name' => 'Частно',
		        'description' => 'Частни проекти'
	        ],
	        [
		        'user_id' => 1,
		        'name' => 'Други',
		        'description' => 'Други'
	        ]
        ]);

	    DB::table('expenses_items')->insert([

		    [
			    'user_id' => 1,
			    'name' => 'Автобус',
			    'description' => 'Автобус до София'
		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Такси',
			    'description' => 'Таксита'
		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Храна - Кауфланд',
			    'description' => 'Пазаруване от Кауфланд'
		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Ток',
			    'description' => 'ЧЕЗ'
		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Телефон',
			    'description' => 'Телефон + Лизинг'
		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Интернет',
			    'description' => 'Нетсърф'
		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Върнат Заем',
			    'description' => ''
		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Храна навън',
			    'description' => 'Хранения навън'
		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Цигари',
			    'description' => ''

		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Алкохол',
			    'description' => ''

		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Гориво',
			    'description' => ''

		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Лакомства',
			    'description' => ''

		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Дрехи',
			    'description' => ''

		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Телевизия',
			    'description' => ''

		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Кредит Банки',
			    'description' => ''

		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Сървъри',
			    'description' => ''

		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Домейни',
			    'description' => ''

		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Стройтелни Матеряли',
			    'description' => ''

		    ],
		    [
			    'user_id' => 1,
			    'name' => 'Други',
			    'description' => ''

		    ]
		    ,
		    [
			    'user_id' => 1,
			    'name' => 'За вкъщи',
			    'description' => ''

		    ]

	    ]);




    }
}
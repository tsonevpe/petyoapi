<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $with = ['item'];

    protected $guarded = ['id'];

    public function item()
    {
        return $this->belongsTo(IncomeItem::class,'income_item_id','id');
    }
}

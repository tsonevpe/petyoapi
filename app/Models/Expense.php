<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $with = ['item'];

    protected $table = 'expenses';

    protected $guarded = ['id'];

    public function item()
    {
        return $this->belongsTo(ExpenseItem::class,'expenses_item_id','id');
    }
}

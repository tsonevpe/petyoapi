<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fitnes extends Model
{
    protected  $table = 'fitness';

    protected $guarded = ['id'];

    protected $casts = [

    	'workout' => 'array'
    ];

}

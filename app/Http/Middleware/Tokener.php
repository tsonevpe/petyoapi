<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\TokenMismatchException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Tokener
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {

            $check = \Tymon\JWTAuth\Facades\JWTAuth::setRequest($request)->parseToken()->check();

            if ($check) {

                return $next($request);

            } else {

                return response()->json(['Unauthorized'],401);

            }


        }catch (TokenInvalidException | TokenMismatchException | TokenBlacklistedException | JWTException $exception) {

            return response()->json(['Unauthorized'],401);
        }

    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Session\TokenMismatchException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\JWTAuth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'api')
    {

        try {

            $check = \Tymon\JWTAuth\Facades\JWTAuth::setRequest($request)->parseToken()->check();

            if ($check) {

                return $next($request);

            } else {

                return response()->json(['Unauthorized'],401);

            }


        }catch (TokenInvalidException | TokenMismatchException | TokenBlacklistedException $exception) {

            return response()->json(['Unauthorized'],401);
        }

       /* dd(\Tymon\JWTAuth\Facades\JWTAuth::setRequest($request)->parseToken($request)->toUser());
        if ($this->auth->guard($guard)->guest()) {
            return response('Unauthorized.', 401);
        }*/

        //return $next($request);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: tsonev
 * Date: 22.09.17
 * Time: 20:10
 */

namespace App\Http\Middleware;
class Cors {


	public function handle($request, \Closure $next)
	{
		$response = $next($request);
		$response->header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
		$response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));
		$response->header('Access-Control-Allow-Origin', '*');
		return $response;
	}
}
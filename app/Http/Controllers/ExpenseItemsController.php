<?php

namespace App\Http\Controllers;

use App\Models\ExpenseItem;

class ExpenseItemsController extends Controller
{
    public function all()
    {

	    $items = ExpenseItem::where('user_id',\Auth::id())->get();

	    $newItems = [];

	    foreach ($items->toArray() as $key => $value) {


		    $newItems[$key] = $value;



		    $newItems[$key]['label'] = $value['name'];
		    $newItems[$key]['value'] = $value['id'];

	    }

	    return response()->json($newItems);
    }
}

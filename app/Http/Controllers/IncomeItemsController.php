<?php

namespace App\Http\Controllers;

use App\Models\IncomeItem;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class IncomeItemsController extends Controller
{
    public function all()
    {
    	$items = IncomeItem::where('user_id',\Auth::id())->get();

    	$newItems = [];

    	foreach ($items->toArray() as $key => $value) {


    		$newItems[$key] = $value;



    		$newItems[$key]['label'] = $value['name'];
		    $newItems[$key]['value'] = $value['id'];

	    }

    	return response()->json($newItems);
    }

    public function create(Request $request)
    {

    }
}
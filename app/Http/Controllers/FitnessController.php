<?php
/**
 * Created by PhpStorm.
 * User: tsonev
 * Date: 18.09.17
 * Time: 22:46
 */

namespace App\Http\Controllers;

use App\Models\Fitnes;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use Tymon\JWTAuth\Facades\JWTAuth;

class FitnessController extends Controller
{
    /*public function upload(Request $request)
    {

        return $this->encryptAndStorePhoto($request->file('photo'));
    }

    public function fetch($id)
    {
    	$fitness = Fitnes::where('id',$id)->first();


    	$decryptPhoto = $this->decryptAndEncodePhoto($fitness);

    	$image = new ImageManager();

    	dd($image->make($decryptPhoto)->encode()->encoded);




    }

    public function encryptAndStorePhoto(UploadedFile $photo)
    {

    	//return response()->json( [$photo->getPathname()]);
        $contents = file_get_contents($photo->getPathname());

        $crypt = app()->make('encrypter');

        $encrypted = Crypt::encryptString(base64_encode($contents));

        $set = Storage::disk('local')->put(md5(time()).'.'.$photo->getClientOriginalExtension(),$encrypted);

        $return = [

            'filename' => md5(time()).'.'.$photo->getClientOriginalExtension(),
            'file_path' => 'app/fitness/'.md5(time()).'.'.$photo->getClientOriginalExtension()
        ];

        $storage = new Fitnes();
        $storage->user_id = \Auth::user()->id;
        $storage->file_location = $return['file_path'];
        $storage->file_type = $photo->getMimeType();

        try {

	        $storage->saveOrFail();

	        $response = [

		        'status' => 'OK',
		        'message' => 'File Uploaded',
		        'id'  => $storage->id,
	        ];

	        return response()->json($response,201);


        } catch (QueryException | \PDOException $exception) {

        	return response()->json([

        		'status' => 'ERROR',
		        'message' => $exception->getMessage()
	        ],500);
        }


    }

    public function decryptAndEncodePhoto(Fitnes $photo)
    {
        $encrypted = File::get(storage_path($photo->file_location));

        $decrypted = Crypt::decryptString($encrypted);

        $manager = [

        	'type' => $photo->type,
	        'image' => $decrypted
        ];

        return $manager;
    }*/


    public function newWorkOut(Request $request)
    {
    	$fitness = new Fitnes();

    	$fitness->workout = $request->json()->all();
    	$fitness->user_id = \Auth::id();

    	try {

    		$fitness->saveOrFail();


    		$arra = [

    			'status' => 'OK',
			    'http_code' => 201,
			    'id' => $fitness->id,
			    'data' => $fitness->workout

		    ];

    		return response()->json($arra,201);
	    } catch (\PDOException | QueryException | \ErrorException $exception) {

    		return response()->json([

    			'status' => 'ERROR',
			    'http_code'   => 500,
			    'message'     => $exception->getMessage()
		    ]);
	    }
    }

    public function fetch(int $id)
    {
    	$data = Fitnes::find($id);

    	$responseArray = [

    		'status' => 'OK',
		    'http_code' => 200,
		    'data' => $data
	    ];

    	return response()->json($responseArray);
    }

    public function update(Request $request,$id)
    {
    	try {

    		$fit = Fitnes::find($id);

    		$fit->update([
    			'workout' => $request->json()->all()
		    ]);

    		return response()->json([

    			'status' => 'OK',
			    'http_code' => 200,
			    'id' => $id,
			    'message' => 'Updated',
			    'data' => $request->json()->all()
		    ]);

	     } catch (\PDOException | QueryException $exception) {

    		return response()->json([

    			'status' => 'ERROR',
			    'http_code' => 400,
			    'message' => $exception->getMessage()
		    ],400);
	    }
    }

    public function all()
    {

    	$workouts = Fitnes::where('user_id',\Auth::id())->get();

    	$workoutsFiltered = [];

    	foreach ($workouts->toArray() as $key => $value)
	    {
	    	$workoutsFiltered[$key] = $value;
	    	$workoutsFiltered[$key]['total_time'] = $value['workout']['boxing'] +
		                                            $value['workout']['jumping'] +
		                                            $value['workout']['walking'] +
		                                            $value['workout']['static_bike'];


	    }

    	return response()->json([

    		'status' => 'OK',
		    'http_code' => 200,
		    'data' => $workoutsFiltered
	    ]);

    }


}
<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ExpensesController extends Controller
{
    public function all()
    {
	    $expenses = Expense::where('user_id',\Auth::id())->get();

	    return response()->json($expenses);
    }

	public function create(Request $request)
	{
		$expense = new Expense();

		$rules = [

			'amount' => 'required|numeric',
			'expenses_item_id' => 'required|numeric',
			'name' => 'required',
		];

		$validator = \Illuminate\Support\Facades\Validator::make($request->json()->all(),$rules);

		if ($validator->fails()){

			return response()->json($validator->messages()->toArray(),422);

		}

		try {



			$expense->user_id =  \Auth::id();
			$expense->amount  = $request->json('amount');
			$expense->name  = $request->json('name');
			$expense->description  = $request->json('description');
			$expense->expenses_item_id  = $request->json('expenses_item_id');

			$expense->saveOrFail();


			return response()->json([

				'status' => 'OK',
				'message' => 'Res Created',
				'id' => $expense->id
			],201);
		} catch (\PDOException | QueryException $exception) {

			return response()->json([

				'status' => 'ERROR',
				'message' => $exception->getMessage()
			],400);
		}
	}

	public function update(Request $request, $id)
	{
		$expense = new Expense();

		try {

			$data = $expense->where('id',$id)->where('user_id',\Auth::id())->firstOrFail();

			$update = $data->update([
				'amount' => $request->json('amount'),
				'name' => $request->json('name'),
				'description' => $request->json('description'),
				'expenses_item_id' => $request->json('expenses_item_id'),

			]);

			return response()->json([
				'status' => 'OK',
				'code' => 200,
				'id' => $data->id,
				'income' => $data
			]);

		} catch (ModelNotFoundException $exception) {

			return response()->json([

				'status' => 'ERROR',
				'code' => 400,
				'message' => $exception->getMessage()
			]);

		}



	}

	public function show($id)
	{
		$expense = new Expense();

		try {

			$data = $expense->where('id',$id)->where('user_id',\Auth::id())->firstOrFail();

			return response()->json([
				'status' => 'OK',
				'code' => 200,
				'expense' => $data
			]);

		} catch (ModelNotFoundException $exception) {

			return response()->json([

				'status' => 'FAIL',
				'code' => 400,
				'message' => 'Income not Found'
			]);

		}
	}
}

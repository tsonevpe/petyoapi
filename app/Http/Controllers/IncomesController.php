<?php
/**
 * Created by PhpStorm.
 * User: tsonev
 * Date: 18.09.17
 * Time: 22:44
 */

namespace App\Http\Controllers;

use App\Models\Income;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class IncomesController extends Controller
{

    public function all()
    {
		$all = Income::where('user_id',\Auth::id())->get();

		return response()->json($all);
    }

    public function create(Request $request)
    {
    	$income = new Income();

	    $rules = [

		    'amount' => 'required|numeric',
		    'income_item_id' => 'required|numeric',
		    'name' => 'required',
		    'description' => ''
	    ];

	    $validator = \Illuminate\Support\Facades\Validator::make($request->json()->all(),$rules);

	    if ($validator->fails()){

		    return response()->json($validator->messages()->toArray(),422);

	    }

    	try {


		    $income->user_id =  \Auth::id();
		    $income->amount  = $request->json('amount');
		    $income->name  = $request->json('name');
		    $income->description  = $request->json('description');
		    $income->income_item_id  = $request->json('income_item_id');

		    $income->saveOrFail();

		    return response()->json([

		    	'status' => 'OK',
			    'message' => 'Res Created',
			    'id' => $income->id
		    ],201);
	    } catch (\PDOException | QueryException $exception) {

    		return response()->json([

    			'status' => 'ERROR',
			    'message' => $exception->getMessage()
		    ],400);
	    }
    }

    public function update(Request $request, $id)
    {
	    $income = new Income();

	    try {

		    $data = $income->where('id',$id)->where('user_id',\Auth::id())->firstOrFail();

		    $update = $data->update([
			    'amount' => $request->json('amount'),
			    'name' => $request->json('name'),
			    'description' => $request->json('description'),
			    'income_item_id' => $request->json('income_item_id'),

			    ]);

		    return response()->json([
			    'status' => 'OK',
			    'code' => 200,
			    'id' => $data->id,
			    'income' => $data
		    ]);

	    } catch (ModelNotFoundException $exception) {

		    return response()->json([

			    'status' => 'FAIL',
			    'code' => 400,
			    'message' => $exception->getMessage()
		    ]);

	    }



    }

    public function show(Request $request,$id)
    {
		$income = new Income();

		try {

			$data = $income->where('id',$id)->where('user_id',\Auth::id())->firstOrFail();

			return response()->json([
				'status' => 'OK',
				'code' => 200,
				'income' => $data
			]);

		} catch (ModelNotFoundException $exception) {

			return response()->json([

				'status' => 'FAIL',
				'code' => 400,
				'message' => 'Income not Found'
			]);

		}
    }
}
<?php

namespace App\Policies;

use App\User;
use App\Models\Income as IncomeModel;
use Illuminate\Auth\Access\HandlesAuthorization;

class Income
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the income.
     *
     * @param  App\User  $user
     * @param  App\Income  $income
     * @return mixed
     */
    public function view(User $user, IncomeModel $income)
    {
        //
    }

    /**
     * Determine whether the user can create incomes.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the income.
     *
     * @param  App\User  $user
     * @param  App\Income  $income
     * @return mixed
     */
    public function update(User $user, IncomeModel $income)
    {
        //
    }

    /**
     * Determine whether the user can delete the income.
     *
     * @param  App\User  $user
     * @param  App\Income  $income
     * @return mixed
     */
    public function delete(User $user, IncomeModel $income)
    {
        //
    }
}

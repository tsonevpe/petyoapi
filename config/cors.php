<?php
/**
 * Created by PhpStorm.
 * User: tsonev
 * Date: 22.09.17
 * Time: 20:23
 */
return [
	/*
	|--------------------------------------------------------------------------
	| Laravel CORS
	|--------------------------------------------------------------------------
	|
	| allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
	| to accept any value.
	|
	*/
	'supportsCredentials' => false,
	'allowedOrigins' => ['*'],
	'allowedHeaders' => ['Content-Type', 'X-Requested-With','Authorization'],
	'allowedMethods' => ['*'], // ex: ['GET', 'POST', 'PUT',  'DELETE']
	'exposedHeaders' => [],
	'maxAge' => 0,
];
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', ['middleware'=>'tokener', function () use ($router) {
    return $router->app->version();
}]);

$route = app()->router;

$route->post('auth/login','LoginController@loginPost');

$route->get('auth/user',[

	'middleware' => 'tokener',function() use($route){

	return Auth::user();

	}]);

$route->group([

    'prefix' => 'income'

],function (){


});

$route->group([
    'prefix' => 'fitness',
	'middleware' => 'tokener'
],function ()use ($route){

    $route->post('create','FitnessController@newWorkOut');
	$route->get('fetch/{id}','FitnessController@fetch');
	$route->post('update/{id}','FitnessController@update');
	$route->get('all','FitnessController@all');

});

$route->group([
	'prefix' => 'income',
	'middleware' => 'tokener'
],function ()use ($route){

	$route->post('create','IncomesController@create');
	$route->get('show/{id}','IncomesController@show');
	$route->post('update/{id}','IncomesController@update');
	$route->get('all','IncomesController@all');

});

$route->group([
	'prefix' => 'income-items',
	'middleware' => 'tokener'
],function () use ($route){

	$route->get('all','IncomeItemsController@all');
});

$route->group([
	'prefix' => 'expense',
	'middleware' => 'tokener'
],function ()use ($route){

	$route->post('create','ExpensesController@create');
	$route->get('show/{id}','ExpensesController@show');
	$route->post('update/{id}','ExpensesController@update');
	$route->get('all','ExpensesController@all');

});

$route->group([
	'prefix' => 'expense-items',
	'middleware' => 'tokener'
],function () use ($route){

	$route->get('all','ExpenseItemsController@all');
});

